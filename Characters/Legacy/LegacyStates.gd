extends "res://Characters/StateMachine.gd"


var mass: = 750


func _ready() -> void:
	add_state("idle")
	add_state("run")
	add_state("jump")
	add_state("fall")

	call_deferred("set_state", states.idle)


func _physics_process(delta: float) -> void:
	motion.y += mass * delta
#	motion = motion.clamped(max_speed)
	motion = parent.move_and_slide(motion, Vector2.UP)

	transition()


puppet func action_set_motion(linear_velocity: Vector2) -> void:
	if state == states.jump or state == states.fall:
		linear_velocity = restrict_y_motion(linear_velocity)
	else:
		linear_velocity = set_y(linear_velocity, 0)

	update_motion(linear_velocity)
	rpc("update_motion", parent.position, motion, true)
