extends Node

# TODO: Rename?
#class_name StateMachine

var state: = -1 setget set_state
var states: = {}
var rev_states: = {}
var motion: = Vector2.ZERO

onready var parent: KinematicBody2D = get_parent()


func _enter_state() -> void:
	pass


func set_state(new_state: int) -> void:
	state = new_state
	_enter_state()


func add_state(state_name: String) -> void:
	var state_id: = states.size()

	states[state_name] = state_id
	rev_states[state_id] = state_name


func update_motion(new_motion: Vector2) -> void:
	# TODO: limit speed here?
	motion = new_motion


# Common utils


func set_x(vector: Vector2, x: float) -> Vector2:
	return Vector2(x, vector.y)


func set_y(vector: Vector2, y: float) -> Vector2:
	return Vector2(vector.x, y)


func restrict_y_motion(linear_velocity: Vector2) -> Vector2:
	if linear_velocity.y < motion.y or linear_velocity.y > 0:
		return set_y(linear_velocity, motion.y)
	return linear_velocity


# Common states


func enter_state_walk() -> void:
	set_state(states.run)
	rpc("enter_state_walk", parent.position, motion.x)


func enter_state_idle() -> void:
	set_state(states.idle)
	rpc("enter_state_idle", parent.position)


func enter_state_jump() -> void:
	set_state(states.jump)
	rpc("enter_state_jump", parent.position, motion)


func enter_state_fall() -> void:
	set_state(states.fall)
	rpc("enter_state_fall", parent.position, motion)


func transition() -> void:
	match state:
		states.jump:
			if parent.is_on_floor():
				if motion.x == 0:
					enter_state_idle()
				else:
					enter_state_walk()
			elif motion.y > 0:
				enter_state_fall()

		states.run:
			if not parent.is_on_floor():
				print("fall: " + str(motion))
				enter_state_fall()

		states.fall:
			if parent.is_on_floor():
				if motion.x == 0:
					enter_state_idle()
				else:
					enter_state_walk()

# Common actions


puppet func action_walk(x_velocity: float) -> void:
	if not parent.is_on_floor():
		return

	var linear_velocity: = set_x(motion, x_velocity)

	update_motion(linear_velocity)
	enter_state_walk()


puppet func action_idle() -> void:
	if not parent.is_on_floor():
		return

	update_motion(Vector2.ZERO)
	enter_state_idle()


puppet func action_jump(linear_velocity: Vector2) -> void:
	# TODO: check velocity
	if state == states.jump:
		update_motion(set_x(motion, linear_velocity.x))
		enter_state_jump()
	elif state == states.fall:
		update_motion(set_x(motion, linear_velocity.x))
		enter_state_fall()
	else:
		update_motion(linear_velocity)
		enter_state_jump()
