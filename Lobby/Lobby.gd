extends Control


func initial_lobby_position() -> Vector2:
	return Vector2(360, 30)


func _ready() -> void:
	get_tree().connect("network_peer_disconnected", self, "_on_player_disconnected")
	call_deferred("_init_server")


func _init_server() -> void:
	# Can we just do something like:
	# var players = SceneTree.new() # ...?
	# Or else... This could be our own child?
	var players: = Node.new()
	players.set_name("Players")
	get_tree().get_root().add_child(players)

	create_server(4242)


func create_server(port: int) -> void:
	var peer = NetworkedMultiplayerENet.new()
	peer.server_relay = false
	peer.create_server(port)
	# TODO: check if server creation failed
	get_tree().set_network_peer(peer)


func _on_player_disconnected(player_id: int) -> void:
	var players: = get_node("/root/Players")
	players.remove_child(players.get_node(str(player_id)))
	rpc("remove_player", player_id)


remote func player_entered_lobby(player_info: Dictionary) -> void:
	print("player entered lobby: " + str(player_info))
	# TODO: This can be called by the remote peer anytime
	# they want, so we need to check stuff here.
	var player_id: = get_tree().get_rpc_sender_id()
	var players: = get_node("/root/Players")

	for player in players.get_children():
		rpc_id(player_id, "spawn_player", _player_info(player))

	if player_info.name == "":
		player_info.name = "(" + str(player_id) + ")"

	_spawn_player(player_id, player_info)


func _player_info(player: KinematicBody2D) -> Dictionary:
	return {
		id = int(player.name),
		name = player.nickname,
		position = player.position,
		character = player.character,
		}


func character_instance(character: String):
	match character:
		"witch":
			return preload("res://Characters/Witch/Witch.tscn").instance()

	return preload("res://Characters/Legacy/Player.tscn").instance()


func _spawn_player(id: int, player_info: Dictionary) -> void:
	var player = character_instance(player_info.character)
	var position: = initial_lobby_position()
	var nickname: String = player_info.name

	player.set_name(str(id))
	player.nickname = nickname
	player.set_network_master(id)
	player.position = position
	get_node("/root/Players").add_child(player)
	rpc("spawn_player", {
		id = id,
		name = nickname,
		position = position,
		character = player_info.character,
		})
